from pathlib import Path
import torchvision

DATA_DIR = Path("/Users/ALAMSHC/torch_data")

celeba_dataset = torchvision.datasets.CelebA(
    str(DATA_DIR), split='train', target_type='attr', download=False)

example = next(iter(celeba_dataset))
print(example)

mnist_dataset = torchvision.datasets.MNIST(str(DATA_DIR), 'train', download=True)
example = next(iter(mnist_dataset))
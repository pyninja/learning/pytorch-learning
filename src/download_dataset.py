import torchvision
from pathlib import Path
import argparse
import requests
import os

HOME = os.environ["HOME"]
TORCH_DATA_PATH = f"{HOME}/datasets"


def download_celeba():
    torchvision.datasets.CelebA(str(TORCH_DATA_PATH), split='train', target_type='attr', download=True)
    print(f"CelebA dataset downloaded at {TORCH_DATA_PATH}")


def download_mnist_dataset():
    torchvision.datasets.MNIST(str(TORCH_DATA_PATH), 'train', download=True)
    print(f"Mnist dataset downloaded at {TORCH_DATA_PATH}")


def download_mpg_dataset():
    data_path = Path(TORCH_DATA_PATH).joinpath("auto-mpg")
    resp = requests.get('http://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data')
    data_path.mkdir(parents=True, exist_ok=True)
    with open(data_path.joinpath("dataset.data"), "w") as file:
        file.write(resp.text)
        print(f"Data downloaded at {data_path}")


def main():
    parser = argparse.ArgumentParser(description="Parse the Dataset name to download")
    parser.add_argument(
        '--dataset',
        type=str,
        help='The dataset name example: celeba',
        required=True
    )
    args = parser.parse_args()
    dataset = args.dataset

    if dataset == "celeba":
        download_celeba()
    elif dataset == "mnist":
        download_mnist_dataset()
    elif dataset == "auto-mpg":
        download_mpg_dataset()
    else:
        raise ValueError(f"{dataset} is not a valid data source name")


if __name__ == "__main__":
    main()


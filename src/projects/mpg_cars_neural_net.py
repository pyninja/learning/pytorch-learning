import logging
import os
from pathlib import Path

import pandas as pd
import torch
from sklearn.model_selection import train_test_split
from torch import nn
from torch.nn.functional import one_hot
from torch.utils.data import DataLoader, TensorDataset
from collections import namedtuple
from dataclasses import dataclass


TrainDataFactory = namedtuple("TrainDataFactory", "features, targets")
TestDataFactory = namedtuple("TestDataFactory", "features, targets")
ModelFactory = namedtuple("ModelUtils", "model, loss_fn, optimizer")


@dataclass
class DataFactory:
    train: TrainDataFactory
    test: TestDataFactory


logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
    level=logging.INFO,
)

# Preprocessing
logging.info("Starting Data Processing!")

HOME = os.environ["HOME"]
TORCH_DATA_PATH = Path(HOME).joinpath("datasets")
DATA_PATH = TORCH_DATA_PATH.joinpath("auto-mpg").joinpath("dataset.data")

COLUMN_SCHEMA = [
    "MPG",
    "Cylinders",
    "Displacement",
    "Horsepower",
    "Weight",
    "Acceleration",
    "Model Year",
    "Origin",
]


def read_df(schema: list) -> pd.DataFrame:
    df = pd.read_csv(
        DATA_PATH,
        names=schema,
        na_values="?",
        comment="\t",
        sep=" ",
        skipinitialspace=True,
    )
    df = df.dropna()
    df = df.reset_index(drop=True)
    return df


def standardize_numeric_columns(df: pd.DataFrame):
    df_train, df_test = train_test_split(df, train_size=0.8, random_state=1)

    numeric_column_names = [
        "Cylinders",
        "Displacement",
        "Horsepower",
        "Weight",
        "Acceleration",
    ]

    df_train_norm, df_test_norm = df_train.copy(), df_test.copy()
    train_stats = df_train.describe().transpose()

    for col_name in numeric_column_names:
        mean = train_stats.loc[col_name, "mean"]
        std = train_stats.loc[col_name, "std"]
        df_train_norm.loc[:, col_name] = (df_train_norm.loc[:, col_name] - mean) / std
        df_test_norm.loc[:, col_name] = (df_test_norm.loc[:, col_name] - mean) / std

    return df_train_norm, df_test_norm


# df_train_norm.tail()

def add_model_year_bucket(df_train_norm: pd.DataFrame, df_test_norm: pd.DataFrame):
    boundaries = torch.tensor([73, 76, 79])
    v = torch.tensor(df_train_norm["Model Year"].values)
    df_train_norm["Model Year Bucketed"] = torch.bucketize(v, boundaries, right=True)

    v = torch.tensor(df_test_norm["Model Year"].values)
    df_test_norm["Model Year Bucketed"] = torch.bucketize(v, boundaries, right=True)
    return df_train_norm, df_test_norm


def encode_categorical_column(df_train_norm, df_test_norm):
    numeric_column_names = [
        "Cylinders",
        "Displacement",
        "Horsepower",
        "Weight",
        "Acceleration",
        "Model Year Bucketed",
    ]
    total_origin = len(set(df_train_norm["Origin"]))
    origin_encoded = one_hot(
        torch.from_numpy(df_train_norm["Origin"].values) % total_origin
    )  # % total_origin is to make zero based origin
    x_train_numeric = torch.tensor(df_train_norm[numeric_column_names].values)
    x_train = torch.cat([x_train_numeric, origin_encoded], 1).float()

    origin_encoded = one_hot(torch.from_numpy(df_test_norm["Origin"].values) % total_origin)
    x_test_numeric = torch.tensor(df_test_norm[numeric_column_names].values)
    x_test = torch.cat([x_test_numeric, origin_encoded], 1).float()

    y_train = torch.tensor(df_train_norm["MPG"].values).float()
    y_test = torch.tensor(df_test_norm["MPG"].values).float()

    data = DataFactory(TrainDataFactory(x_train, y_train), TestDataFactory(x_test, y_test))
    return data


def create_torch_data_set(x_train, y_train):
    train_ds = TensorDataset(x_train, y_train)
    batch_size = 8
    torch.manual_seed(1)
    train_dl = DataLoader(train_ds, batch_size, shuffle=True)
    return train_dl


def build_model_layers(x_train):
    hidden_units = [8, 4]
    input_size = x_train.shape[1]
    all_layers = []
    for hidden_unit in hidden_units:
        layer = nn.Linear(input_size, hidden_unit)
        all_layers.append(layer)
        all_layers.append(nn.ReLU())
        input_size = hidden_unit

    all_layers.append(nn.Linear(hidden_units[-1], 1))
    model = nn.Sequential(*all_layers)

    loss_fn = nn.MSELoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001)

    return ModelFactory(model, loss_fn, optimizer)


def train(train_dl, model_factory: ModelFactory):
    torch.manual_seed(1)
    num_epochs = 200
    log_epochs = 20
    model, loss_fn, optimizer = model_factory
    for epoch in range(num_epochs):
        loss_hist_train = 0
        for x_batch, y_batch in train_dl:
            pred = model(x_batch)[:, 0]
            loss = loss_fn(pred, y_batch)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            loss_hist_train += loss.item()
        if epoch % log_epochs == 0:
            logging.info(f"Epoch {epoch} loss: {loss_hist_train/len(train_dl):.4f}")


def test(dataset: TestDataFactory, model_factory: ModelFactory):
    with torch.no_grad():
        pred = model_factory.model(dataset.features.float())[:, 0]
        loss = model_factory.loss_fn(pred, dataset.targets)
        logging.info(f"Test MSE: {loss.item():.4f}")
        logging.info(f"Test MAE: {nn.L1Loss()(pred, dataset.targets).item():.4f}")


def main():
    raw_df = read_df(COLUMN_SCHEMA)
    train_norm_df, test_norm_df = standardize_numeric_columns(raw_df)
    train_norm_df, test_norm_df = add_model_year_bucket(train_norm_df, test_norm_df)
    datasets = encode_categorical_column(train_norm_df, test_norm_df)
    train_dl = create_torch_data_set(datasets.train.features, datasets.train.targets)
    model_factory = build_model_layers(datasets.train.features)
    train(train_dl, model_factory)
    test(datasets.test, model_factory)


if __name__ == "__main__":
    main()

